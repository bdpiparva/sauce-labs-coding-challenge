package com.coding.saucelabs.reflection;

import android.os.IBinder;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;
import static java.util.Collections.EMPTY_LIST;

/**
 * This class is responsible to make an reflective calls to the @code{android.os.ServiceManager}
 * interface
 */
public class ReflectiveServiceManager {
    private static final String TAG = ReflectiveServiceManager.class.getSimpleName();
    private final Class serviceManageClass;

    public ReflectiveServiceManager() {
        try {
            serviceManageClass = Class.forName("android.os.ServiceManager");
        } catch (ClassNotFoundException e) {
            throw new RuntimeClassNotFoundException(e);
        }
    }

    /**
     * The method will get all services by calling listServices on @code{android.os.ServiceManager}
     * class
     *
     * @return list of all services
     */
    public List<String> listServices() {
        try {
            Method listService = serviceManageClass.getMethod("listServices");
            String[] servicesName = (String[]) listService.invoke(serviceManageClass);
            Log.i(TAG, format("Found %d services.", servicesName.length));
            return Arrays.asList(servicesName);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            Log.e(TAG, format("Failed to list all the service. Cause: %s", e.getMessage()));
            return EMPTY_LIST;
        }
    }

    /**
     * The method return service from @code{android.os.ServiceManager} with given name.
     *
     * @param serviceName: service to get
     * @return an service object
     */
    public IBinder getService(String serviceName) {
        try {
            Method getService = serviceManageClass.getMethod("getService", String.class);
            return (IBinder) getService.invoke(serviceManageClass, serviceName);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            Log.e(TAG, format("Failed get service with name %s. Cause: %s", serviceName, e.getMessage()));
            return null;
        }
    }
}
