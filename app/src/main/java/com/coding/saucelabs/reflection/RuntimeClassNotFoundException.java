package com.coding.saucelabs.reflection;

public class RuntimeClassNotFoundException extends RuntimeException {
    public RuntimeClassNotFoundException(ClassNotFoundException e) {
        super(e);
    }
}
