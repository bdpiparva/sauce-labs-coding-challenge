package com.coding.saucelabs.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.coding.saucelabs.reflection.ReflectiveServiceManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This is logging service to log output of all the tasks given
 */
public class LoggingService extends Service {
    private static final String TAG = LoggingService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ReflectiveServiceManager serviceManager = new ReflectiveServiceManager();

        // Task 2. Print all services on logcat
        Log.i(TAG, "=============================== All Services ===============================");
        for (String service : serviceManager.listServices()) {
            Log.i(TAG, service);
        }
        Log.i(TAG, "======================================================================");


        // Task 3. Get batterystatus service
        IBinder service = serviceManager.getService("batterystats");

        try {
            String serviceInterface = service.getInterfaceDescriptor();
            Log.i(TAG, "Interface for 'batterystats' is : " + serviceInterface);

            Object batteryStats = getServiceAsInterface(service);

            // Task 4. Call charging method and print it on logcat
            Log.i(TAG, "Charging: " + isCharging(batteryStats));
        } catch (RemoteException e) {
            Log.e(TAG, e.getMessage());
        }

        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * The method is responsible for calling 'isCharging' method on IBatteryStats class
     * Note: 'charging' method or field is not available on the class IBatteryStats,
     *       hence I have used method 'isCharging' instead.
     * @return: whether device is charging or not
     */
    private Object isCharging(Object batteryStats) {
        try {
            Method charging = batteryStats.getClass().getMethod("isCharging");
            return charging.invoke(batteryStats);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The method is responsible to get an interface for given batterystats service
     * @param service: Batterystats service
     * @return IBatteryStats
     */
    private Object getServiceAsInterface(IBinder service) {
        try {
            Class stubClass = Class.forName("com.android.internal.app.IBatteryStats$Stub");
            Method asInterface = stubClass.getMethod("asInterface", IBinder.class);
            return asInterface.invoke(stubClass, service);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

}
