# Coding Challenge

## Apk to be run
- Please download APK from [here](/apk)

## The adb command to start the the service on the emulator
- Please run following command to start the service

```bash
adb shell am startservice -n com.coding.saucelabs/.service.LoggingService
```

- Please run following command to force stop the service

```bash
adb shell am force-stop com.coding.saucelabs
```

## About the solution
I have added the comments on appropriate classes and methods. The apk does not contain any activities
as problem statement does not require any. There are two main classes: 

1. [LoggingService](/app/src/main/java/com/coding/saucelabs/service/LoggingService.java)
    - This is used as an entrypoint of the problem and it logs the all tasks output to logcat

2. [ReflectiveServiceManager](/app/src/main/java/com/coding/saucelabs/reflection/ReflectiveServiceManager.java)
    - This is used to make reflective calls to the `ServiceManager` class
       
        
## Please explain any items that you were unable to solve
I think I have solved all give tasks with one minor exception in task 5. I was not able to find a 
'charging' method on [IBatteryStats](https://android.googlesource.com/platform/frameworks/base.git/+/master/core/java/com/android/internal/app/IBatteryStats.aidl) but found the `isCharging` method instead and have used that. 